import QtQuick 2.0
import QtQuick.Layouts 1.2
import QtQuick.Controls 1.4 as Controls
import Material 0.1
import Material.ListItems 0.1 as ListItem

BottomSheet {
    id: cheatSheet
    height: Units.gu(8)
    property string descriptionText: ''
    property var cheatSheetItems: [
        { "description" : "Find text", "command" : "grep", "example" : "grep -v -x -d -m23", "docs" : "grep -v -x -d -m23" },
        { "description" : "Search for text in file", "command" : "grep", "example" : "grep -v -x", "docs" : "grep -v -x -d -m23" },
        { "description" : "Search for text in file", "command" : "grep", "example" : "grep -v -x", "docs" : "grep -v -x -d -m23" }
    ]

    ColumnLayout {
        anchors.fill: parent
        spacing: Units.dp(6)
        Card {
            id: toolbar
            elevation: 2
            Layout.preferredHeight: searchField.implicitHeight + Units.dp(5)
            Layout.maximumHeight: Units.gu(1)
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignBottom | Qt.AlignLeft
            TextField {
                id: searchField
                placeholderText: "Search"
                floatingLabel: true
                anchors.margins: Units.dp(5)
                anchors.fill: parent
            }
        }

        Card {
            id: selectionArea
            Layout.fillWidth: true
            Layout.preferredHeight: Units.gu(3)
            Layout.minimumHeight: Units.gu(1)
            elevation: 1
            ListView {
                anchors.fill: parent
                model: cheatSheetItems
                delegate: ListItem.Subtitled {
                    id: cheatDelegate
                    text: modelData.description
                    subText: modelData.command
                    secondaryItem: Switch {
                        id: enablingSwitch
                        checked: cheatDelegate.ListView.isCurrentItem
                        anchors.verticalCenter: parent.verticalCenter
                    }

                    onClicked: {
                        ListView.view.currentIndex = index;
                        cheatSheet.descriptionText = modelData.example;
                    }
                    action: Icon {
                        anchors.centerIn: parent
                        name: "image/flash_on"
                        size: Units.dp(32)
                    }
                }
            }
        }

        Controls.TextArea {
            id: contentView
            text: cheatSheet.descriptionText
            readOnly: true
            wrapMode: TextEdit.WrapAtWordBoundaryOrAnywhere
            selectByMouse: true

            Layout.fillHeight: true
            Layout.fillWidth: true
        }
    }
}
