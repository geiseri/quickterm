import QtQuick 2.4
import QtQuick.Controls 1.3 as Controls
import QtQuick.Window 2.2 as Window
import QtQuick.Dialogs 1.2 as Dialogs
import QMLTermWidget 1.0
import Material 0.1


ApplicationWindow {
    id: mainWindow
    title: mainsession.title
    width: Units.gu(10)
    height: Units.gu(10)
    visible: true

    theme {
        primaryColor: Palette.colors["grey"]["500"]
        primaryDarkColor: Palette.colors["grey"]["700"]
        accentColor: Palette.colors["blueGrey"]["500"]
        tabHighlightColor: "white"
    }

    FontLoader {
        id: inconsolata
        source: "qrc:/Inconsolata-Regular.ttf"
    }

    initialPage: Page {
        title: mainsession.title

        actionBar.maxActionCount: 6

        actions: [
            Action {
                iconName: "image/flash_on"
                name: "Scratch Pad"
                onTriggered: scratchSheet.open()
            },

            Action {
                iconName: "image/flash_on"
                name: "Quick Links"
                onTriggered: actionSheet.open()
            },

            Action {
                iconName: "content/content_copy"
                name: "Copy"
                onTriggered: terminal.copyClipboard();
            },

            Action {
                iconName: "content/content_paste"
                name: "Paste"
                onTriggered: terminal.pasteClipboard();
            },

            Action {
                iconName: "action/search"
                name: "Search"
            },

            Action {
                iconName: "action/settings"
                name: "Settings"
                onTriggered: messageDialog.show("cool")
            },

            Action {
                iconName: "navigation/close"
                name: "Close"
                onTriggered: mainWindow.close();
            }
        ]

        QMLTermWidget {
                id: terminal
                anchors.fill: parent
                font.family: inconsolata.name
                font.pixelSize: Units.dp(12)
                colorScheme: "cool-retro-term"
                session: QMLTermSession{
                id: mainsession
                    initialWorkingDirectory: "$HOME"
                    onFinished: mainWindow.close();
                }
                onTerminalUsesMouseChanged: console.log(terminalUsesMouse);
                onTerminalSizeChanged: console.log(terminalSize);

                QMLTermScrollbar {
                    terminal: terminal
                    width: Units.dp(20)
                    Rectangle {
                        opacity: 0.4
                        anchors.margins: Units.dp(5)
                        radius: width * 0.5
                        anchors.fill: parent
                    }
                }
                Component.onCompleted: mainsession.startShellProgram();
            }
            Component.onCompleted: {
                console.log("PID: %1".arg(mainsession.getShellPID()));
                terminal.forceActiveFocus();
            }
    }

    CheatSheet{
        id: scratchSheet

    }

    BottomActionSheet {
        id: actionSheet

        title: "Scratch Pad"

        actions: [
            Action {
                iconName: "social/share"
                name: "Share"
                onTriggered: mainsession.sendText("foo\n")
            }
        ]
    }

    Dialog {
        id: messageDialog
        title: qsTr("May I have your attention, please?")

        function show(caption) {
            messageDialog.text = caption;
            messageDialog.open();
        }
    }

    Component.onCompleted: {
        console.log("Screen density: %1 %2 %3".arg(Units.pixelDensity).arg(Units.dp(12)).arg(Units.gu(1)));
    }
}
